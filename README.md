# mknet

## Installation

```bash
sudo pip3 install git+https://git.deuxfleurs.fr/trinity-1686a/mknet
```

## Usage

```bash
sudo rm -rf /tmp/garage-testnet/ # always start by deleting previous run
sudo mknet create ./config.yml
sudo mknet run-all example/deploy_garage.sh
sudo mknet run dc1:dc1s1 garage -c /tmp/garage-testnet/dc1/dc1s1/garage.toml status
sudo mknet destroy
```

## Instrumented daemons

If you want to use the scripts provided in the `example` folder,
you must add to your path some tools.

### Garage (`deploy_garage.sh`)

```bash
# see versions on https://garagehq.deuxfleurs.fr/_releases.html
export GRG_ARCH=x86_64-unknown-linux-musl
export GRG_VERSION=v0.5.0

sudo wget https://garagehq.deuxfleurs.fr/_releases/${GRG_VERSION}/${GRG_ARCH}/garage -O /usr/local/bin/garage
sudo chmod +x /usr/local/bin/garage

garage help
```

### Minio (`deploy_minio.py`)

```
sudo wget https://dl.min.io/server/minio/release/linux-amd64/minio -O /usr/local/bin/minio
sudo chmod +x /usr/local/bin/minio
```

